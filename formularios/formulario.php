<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, shrink-to-feet=no">
    <link rel="stylesheet" href="formulario.css">
    <title>Formularios</title>
</head>
<body>
    <form action="registro.php" method="post">
<section class="formulario">
    <h3>Formulario de Registro</h3>
    <input class="form-control" type="text" name="no_cuenta" placeholder="No. cuenta">
    <input class="form-control" type="text" name="nombre" placeholder="nombre">
    <input class="form-control" type="text" name="primer_apellido" placeholder="apellido paterno">
    <input class="form-control" type="text" name="segundo_apellido" placeholder="apellido materno">
    <input class="form-control" type="date" name="fecha">
    <div>
        <label for="">Hombre</label>	
        <p><input class="form-radio1" type="radio" name="genero_opcion" value="Hombre"></p>
        <br>
        <label for="">Mujer</label>
        <p><input class="form-radio2" type="radio" name="genero_opcion" value="Mujer"></p>
        <br>
        <label for="">Otro</label>
        <p><input class="form-radio3" type="radio"name="genero_opcion" value="Otro"></p>
        <br>
    </div>
    <input class="form-control" type="password" name="contrasena" placeholder="contrasena">
    <input class="button" type="submit" value="Ingresar datos">
    </form>
    <p> <a href="./login.php">¿Ya tengo cuenta?</a></p>
</section>
</body>
</html>
