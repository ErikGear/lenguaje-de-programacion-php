//Erik Eduardo Galindo Rosales

//Realizar una expresión regular que detecte emails correctos.
/^[a-zA-Z0-9]{0,25}@(yahoo|hotmail|gmail)\.com$\


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
/^([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM](AS|BC|BS|CC|CL|CM|CS|CH|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9A-Z]\d)$/i

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
/^[a-zA-Z0-9]{51}+[a-zA-Z0-9]$/

//Crear una expresion regular para detectar números decimales.
/^([0-9])+\.([0-9])+$/

//Crea una funcion para escapar los simbolos especiales.
<?php
function simbolosEspeciales($caracteresCE){
   $patron = $caracteresCE;
    if(preg_match("/./", $patron)) //expresion regular con la funcion preg_match
    {
     echo "contiene caracteres especiales";
    } else {
        echo "No contiene caracteres especiales";
    }
}
$expresionCE ="#$%*>/vamos_-/.~aver";
simbolosEspeciales($expresionCE);

function emailCorrecto($caracteresCE){
    $patron = $caracteresCE;
     if(preg_match("[a-zA-Z0-9]{0,25}@(hotmail|gmail|outlook)\.com", $patron)) 
     {
      echo "contiene caracteres especiales";
     } else {
         echo "No contiene caracteres especiales";
     }
 }
 $expresionCorreo ="erikgalindo23@gmail.com";
 
 emailCorrecto($expresionCorreo);

function curpCorrecto($curpCorrecto){
    $patron = $curpCorrecto;
     if(preg_match("([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM](AS|BC|BS|CC|CL|CM|CS|CH|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9A-Z]\d)", $patron)) 
     {
      echo "contiene caracteres especiales";
     } else {
         echo "No contiene caracteres especiales";
     }
 }
 $expresionCurp ="MAHJ280603MSPRRV09";
 
 curpCorrecto($expresionCurp);


function numeroDecimal($numero){
    $patron = $numero;
     if(preg_match("([0-9])+\.([0-9])+", $patron)) 
     {
      echo "es un numero decimal";
     } else {
         echo "No es un numero decimal";
     }
 }
 $numero ="0.6";
 
 curpCorrecto($numero);

?>




